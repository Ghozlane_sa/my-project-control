import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
})
export class ContactComponent {
  @Input() activeIndex: any;
  @Input() handleOnClickProp: any;

  handleOnClick = (index: number) => {
    this.handleOnClickProp(index);
  };
}
