import { Component } from '@angular/core';

@Component({
  selector: 'app-home-default',
  templateUrl: './home-default.component.html',
  styleUrls: ['./home-default.component.css'],
})
export class HomeDefaultComponent {}
