import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  activeIndex = 0;

  handleOnClick = (index: number) => {
    console.log({ index });
    this.activeIndex = index;
  };

  canActive = (index: number) => {
    return Boolean(index === this.activeIndex);
  };
}
