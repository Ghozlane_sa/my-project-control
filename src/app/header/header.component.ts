import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent {
  @Input() handleOnClickProp: any;
  @Input() canActiveProp: any;

  handleOnClick = (index: number) => {
    this.handleOnClickProp(index);
  };
  canActive = (index: number) => {
    return this.canActiveProp(index);
  };
}
